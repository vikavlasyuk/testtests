package ua.pp.krotov;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.openqa.selenium.By.xpath;

public class Testbase {

    public void setUp() {

        String baseUrl = "https://uchebnik-stable.mos.ru";
        Configuration.browser = "chrome";
        System.setProperty("selenide.browser", "chrome");
    }

    public static void customerWait(String xpath) {//метод подождать икспас
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 8);
        wait.until(ExpectedConditions.presenceOfElementLocated(xpath(xpath)));
    }

    public static void login () {

     open("https://uchebnik-stable.mos.ru");
     $("#lpLogin").setValue("eomdev@altarix.ru");
     $("#lpPassword").setValue("uuFZeCz1");
     $("#lpSubmit").click();
     }
}


