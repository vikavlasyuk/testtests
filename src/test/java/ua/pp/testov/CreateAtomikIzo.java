package ua.pp.testov;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;

import java.io.File;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.xpath;
import static ua.pp.testov.Testbase.customerWait;
import static ua.pp.testov.Testbase.login;

public class CreateAtomikIzo {
    @Test
    public void CreateAtomikIzo()
    {
        login();
        open("https://uchebnik-stable.mos.ru/material/new?materialType=image");
        $x("//input[@id='new']").uploadFile(new File("src/test/resources/feelings.jpeg"));
        $x("//textarea[@type='text']").setValue("Этот атомик создан автоматически");
        $x("//span[text()='Тип материала *']").click();
        $x("//span[text()='Чертёж']").click();
        $(xpath("//span[@class='searchPlaceholder-2977142724 searchPlaceholderBeforeSelect-616748580']")).click();//блок предмет ,выпадающий список
        $(xpath("//span[text()='Химия']")).click();
        $(xpath("//label[contains(text(),'ООО')]")).click();
        customerWait("//span[text()='Добавить КЭС']");
        $(xpath("//span[text()='Добавить КЭС']")).click();
        $(xpath("//label[contains(@for,'3 Вода. Растворы')]/child::div")).click();
        $(xpath("//span[text()='Добавить']")).click();
        $(xpath("//span[contains(@class, 'timeToStudySeparator')]/preceding-sibling::input")).setValue("00");
        $(xpath("//span[contains(@class, 'timeToStudySeparator')]/following-sibling::input")).setValue("59");
        $(xpath("//div[@id='Являюсь автором материала-checkbox']")).click();
        customerWait("//div[text()='Автор документа *']");
        $(xpath("//div[text()='Автор документа *']/following-sibling::input")).setValue("Джек Лондон");
        $(xpath("//div[text()='Ссылка на первоисточник *']/following-sibling::input")).setValue("https://uchebnik-stable.mos.ru");
        $(xpath("//span[text()='Выбрать папку']")).click();
        customerWait("//span[text()='Моя корзина']");
        $(xpath("//span[text()='Моя корзина']")).click();
        customerWait("//span[text()='Сохранить и посмотреть']");
        $(byXpath("//span[text()='Сохранить и посмотреть']")).click();
        customerWait("//div[text()='ID: ']");
        customerWait("//div[text()='ID: ']");
        $(xpath("//div[text()='ID: ']"));
        $(xpath("//div[text()='Химия']"));//проверка, что в атомик при создании данные подтянулись
        $(xpath("//div[text()='OOO']"));
        $(xpath("//div[text()='Вода. Растворы']"));
        $(xpath("//div[text()='Базовый']"));
        $(xpath("//div[text()='59 секунд']"));
        $(xpath("//span[text()='Пестов Артём Павлович']"));
        $(xpath("//div[text()='Джек Лондон']"));
        $(xpath("//a[text()='https://uchebnik-stable.mos.ru']"));
        Selenide.close();
    }
}
